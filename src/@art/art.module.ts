import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ArtInputComponent } from './components/art-input/art-input.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { ArtToolbarComponent } from './components/art-toolbar/art-toolbar.component';
import {InitialsPipe} from './pipes/initials.pipe';
import { RoundNumberPipe } from './pipes/round-number.pipe';
import { LastUpdatePipe } from './pipes/last-update.pipe';
import {SortPipe} from './pipes/sort.pipe';



@NgModule({
  declarations: [
    ArtInputComponent,
    ArtToolbarComponent,
    InitialsPipe,
    RoundNumberPipe,
    LastUpdatePipe,
    SortPipe
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule
    ],
  exports: [
    ArtInputComponent,
    ArtToolbarComponent,
    InitialsPipe,
    RoundNumberPipe,
    LastUpdatePipe,
    SortPipe

  ]
})
export class ArtModule { }

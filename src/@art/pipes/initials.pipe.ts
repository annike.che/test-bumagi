import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'initials'
})
export class InitialsPipe implements PipeTransform {

  transform(value: string, ): string {

     return (value.length > 0) ? `${value.charAt(0)}` : '';


  }

}

import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';
import 'moment/min/locales';

@Pipe({
  name: 'lastUpdate'
})
export class LastUpdatePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    moment.relativeTimeThreshold('s', 40);
    moment.relativeTimeThreshold('ss', 2);

    moment.updateLocale('ru', {
      relativeTime: {
        past: (localeSpec) => {
          if (localeSpec === 'только что') return localeSpec;
          return `${localeSpec} назад`;
        },
        s:  'только что'
      }
    });
    return moment(value).lang('ru').fromNow();
  }

}

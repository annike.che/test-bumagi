import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'roundNumber'
})
export class RoundNumberPipe implements PipeTransform {

  transform(value: number|string, arg: number|string ): any {
    value = +value;
    const count = +arg || 1;
    value = value.toFixed(count);
    var parts = value.toString().split('.');
    parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, '');
    return parts.join('.');
  }

}

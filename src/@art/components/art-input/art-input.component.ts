import {Component, forwardRef, Input, OnInit, Self} from '@angular/core';
import {
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
  Validator
} from '@angular/forms';

@Component({
  selector: 'app-art-input',
  templateUrl: './art-input.component.html',
  styleUrls: ['./art-input.component.scss'],

  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      multi: true,
      useExisting: forwardRef(() => ArtInputComponent)
    }
  ]
})
export class ArtInputComponent
  implements OnInit, ControlValueAccessor{

  private _value;

  @Input() type = 'text';

  @Input() changeTypeInput = false;

  get value() {
    return this._value;
  }

  @Input()
  set value(val) {
    this._value = val;
    this.onChange(this._value);
  }

  private showPass = false;

  constructor() {
  }

  ngOnInit() {
  }

  changeViewPass() {
    this.showPass = !this.showPass;
    this.type = this.showPass ? 'text' : 'password';
  }

  onChange(_: any) {}
  onTouch(_: any) {}

  writeValue(value: any) {
    this.value = value;
  }

  registerOnChange(fn) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouch = fn;
  }



}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AuthComponent} from './page/auth/auth.component';
import {PersonsComponent} from './page/persons/persons.component';


const routes: Routes = [
  { path: '', redirectTo: 'auth', pathMatch: 'full'  },
  { path: 'auth', component: AuthComponent },
  {
    path: 'persons',
    loadChildren: () => import('./page/persons/persons.module').then(m => m.PersonsModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

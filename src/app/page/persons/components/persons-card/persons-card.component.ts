import {Component, Input, OnInit} from '@angular/core';
import {PersonsService} from '../../persons.service';

@Component({
  selector: 'app-persons-card',
  templateUrl: './persons-card.component.html',
  styleUrls: ['./persons-card.component.scss']
})
export class PersonsCardComponent implements OnInit {

  _data = {
    image: this.getImage()
  };

  //numberIndex: number;

  @Input() index: number;

  @Input() set item(value: any) {

    console.log('index', this.index);

    value.image =  this._data.image ;
    value.index = this.index;
    this._data = value;

  }

  get item(): any {

    return this._data;
  }

  constructor(
    private personsService: PersonsService
  ) { }

  ngOnInit() {

  }

  getImage() {
   return `assets/image/avatar_${Math.floor(Math.random() * 3) + 1}.png`;
  }

}

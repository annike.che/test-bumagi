import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {PersonsService} from '../../persons.service';
import {ActivatedRoute} from '@angular/router';
import {Subject, Subscription} from 'rxjs';
import {takeUntil} from 'rxjs/operators';


@Component({
  selector: 'app-persons-list',
  templateUrl: './persons-list.component.html',
  styleUrls: ['./persons-list.component.scss']
})
export class PersonsListComponent implements OnInit, OnDestroy {

  persons: any;
  status: number;
  interval: any;
  hasError: boolean;
  loading = true;
  message = 'При загрузке данных произошла ошибка';

  private unsubscribe  = new Subject();
  private subscription: Subscription = new Subscription();


  constructor(private personsService: PersonsService,
              private route: ActivatedRoute) {
  }

  ngOnInit() {
    this.route.params.subscribe(res => {
      if (res.hasOwnProperty('status')) {
        this.status = this.personsService.getStatusId(res.status);
      } else {
        this.status = undefined;
      }

      this.refreshData();
      if (this.interval) {
        clearInterval(this.interval);
      }
      this.interval = setInterval(() => {
        this.refreshData();
      }, 5000);

      this.personsService.data$.pipe(
        takeUntil(this.unsubscribe)
      ).subscribe(res => {
        if (res.done === false) {
          if (!this.persons) {
            this.hasError = true;
          }

        } else {
          this.persons = res;
          this.hasError = false;
        }
        this.loading = false;

      }, error => {
        if (!this.persons) {
          this.hasError = true;
        }

        this.loading = false;
      });

    });
  }

  getPersons() {
  // console.log('this.statuw', this.status);

    this.personsService.getUsers(this.status)
      .subscribe(
        res => {
          if (res.done === false) {
            if (!this.persons) {
              this.hasError = true;
            }

          } else {
            this.persons = res;
            this.hasError = false;
          }

          this.loading = false;


        }, error => {
          if (!this.persons) {
            this.hasError = true;
          }

          this.loading = false;
        }
      );
  }

  refreshData() {
    this.personsService.updateData(this.status)
      .pipe(
        takeUntil(this.unsubscribe)
      )
      .subscribe();
  }

  doAction() {
    /*this.subscription.add(
      this.personsService.doAction()
        .subscribe(result => {
          if (result === true) {
            this.refreshData();
          }
        })
    );*/
  }

  trackByFn(index, item) {
    //console.log('item', item.id);
    return index; // or item.id
  }

  orderByRowNum = (a,b) => {
    console.log('A', a);
    console.log('b', b);
    return a.key;
  };

  ngOnDestroy(): void {
    this.unsubscribe.next();
    this.unsubscribe.complete();
    clearInterval(this.interval);
  }
}

import { Injectable } from '@angular/core';
import {PersonsModule} from './persons.module';
import {BehaviorSubject, interval, Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {environment} from '../../../environments/environment';
import {delay, flatMap, map, tap} from 'rxjs/operators';

@Injectable()

export class PersonsService {
  urlPrefix: string;
  statuses: Array<any>;

  private dataSubject: BehaviorSubject<any[]> = new BehaviorSubject([]);

  data$: Observable<any> = this.dataSubject.asObservable();

  constructor(private _httpClient: HttpClient) {
    this.urlPrefix = environment.apiUrl;
    this.statuses = [
      {
        id: 0,
        name: 'Активен',
        code: 'active'
      },
      {
        id: 1,
        name: 'Приостановлен',
        code: 'paused'
      },
      {
        id: 2,
        name: 'Заблокирован',
        code: 'blocked'
      },

    ];
  }

  updateData(status?: number): Observable<any> {
    return this.getUsers(status)
      .pipe(
        tap ((data) => {
          this.dataSubject.next(data);
        })
      );
  }

  getUsers( status?: number): Observable<any> {
    const url = `${this.urlPrefix}/users`;
    let params;

    if (status >= 0) params = {status};

    return this._httpClient.get(url, {params, observe: 'response'})
      .pipe(
        map(resp => {
            console.log('resp', resp);
            if (resp.status === 200 && resp.ok) {
              if (Array.isArray(resp.body)) {
                return resp.body;
              } else {
                return Object.assign(resp.body, {done: false});
              }
            } else {

            }
          }
        )
      );
  }

  getIcon(name: string): Observable<any> {
    const url = `${this.urlPrefix}${name}`;

    return  this._httpClient.get(url, { responseType: 'blob' });
  }

  getStatuses() {
    return this.statuses;
  }

  getStatusName(id: number) {
    let name = '';

    this.statuses.forEach( ( item ) => {
      if (item.id === id) {
        name = item.name;
      }
    } );

    return name;
  }

  getStatusId(code: string) {
    let id;

    this.statuses.forEach( ( item ) => {
      if (item.code === code) {
        id = item.id;
      }
    } );

    return id;
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PersonsComponent} from './persons.component';
import {RouterModule, Routes} from '@angular/router';
import {PersonsService} from './persons.service';
import {PersonsListComponent} from './components/persons-list/persons-list.component';
import {PersonsCardComponent} from './components/persons-card/persons-card.component';
import {ArtModule} from '../../../@art/art.module';
import { StatusNamePipe } from './pipes/status-name.pipe';

const routes: Routes = [
  { path: '', component: PersonsComponent, children: [
      {path: '', component: PersonsListComponent},
      {path: ':status', component: PersonsListComponent}
    ]
  }
];


@NgModule({
  declarations: [
    PersonsComponent,
    PersonsListComponent,
    PersonsCardComponent,
    StatusNamePipe
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    ArtModule,
  ],
  providers: [
    PersonsService
  ],
})
export class PersonsModule { }

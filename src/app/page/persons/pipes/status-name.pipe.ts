import { Pipe, PipeTransform } from '@angular/core';
import {PersonsService} from '../persons.service';

@Pipe({
  name: 'statusName'
})
export class StatusNamePipe implements PipeTransform {

  constructor(private personsService: PersonsService) {

  }



  transform(id: number): any {
    return this.personsService.getStatusName(id);
  }

}

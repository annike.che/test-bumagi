import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { environment } from '../../../environments/environment';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {map, tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  urlPrefix: string;
  private authToken: string;


  constructor(private _httpClient: HttpClient) {
    this.urlPrefix = environment.apiUrl;
    console.log('authtoken', this.authToken);
  }
  setToken(token: string): void {
    this.authToken = token;
    localStorage.setItem('auth', token);
  }
  getToken(): string {
    this.authToken = this.authToken || localStorage.getItem('auth');
    return this.authToken;
  }
  clearToken(): void {
    localStorage.removeItem('auth');
    this.authToken = undefined;
  }





  auth( authDate ): Observable<any> {
    const url = `${this.urlPrefix}/auth`;
    return this._httpClient.post(url, authDate,  {observe: 'response'})
      .pipe(
        map(resp => {
            if ( resp.status === 200 && resp.ok ) {
              const header = resp.headers.get('Authorization');
              if (header) {
                this.setToken( header);
                return Object.assign( resp.body, {done: true});
            } else {
                return Object.assign( resp.body, {done: false});
              }
            }
          }
        )
      );

  }
}

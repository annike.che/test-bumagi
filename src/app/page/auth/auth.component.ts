import {Component, forwardRef, OnInit} from '@angular/core';
import {AuthService} from './auth.service';
import {
  ControlContainer, ControlValueAccessor,
  FormBuilder,
  FormGroup,
  FormGroupDirective,
  NG_VALUE_ACCESSOR,
  Validators
} from '@angular/forms';
import {Router} from '@angular/router';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
})
export class AuthComponent implements OnInit {

  loginForm: FormGroup;

  error: string;
  hasError: boolean;

  isLoading: boolean;

  constructor(
    private authService: AuthService,
    private fb: FormBuilder,
    private router: Router,
  ) {

  }

  ngOnInit() {
    this.loginForm = this.fb.group({
      login: ['test@example.com',
      Validators.required
      ],
      password: ['1q2w3e',
      Validators.required
      ]
    });
  }

  login() {
    //'test@example.com', '1q2w3e'
    this.hasError = false;
    this.isLoading = true;
    this.authService.auth(this.loginForm.value)
      .subscribe(
        resp => {
         console.log(resp);
         if ( resp.done ) {
          console.log('авторизация прошла успешно');
          this.router.navigate(['/persons']);
          } else {
          this.hasError = true;
          this.error = resp.message || 'При авторизации произошла ошибка. Попробуйте позже ';
          }
        },
        error => {
          //console.log(error);
          this.hasError = true;
          this.error = error.error.message || 'Что-то пошло не так. Попробуйте позже';
        })
      .add( () => {
        this.isLoading = false;
      });
  }
}
